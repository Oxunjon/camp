<?php
return [
    'class' => 'codemix\localeurls\UrlManager',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
//    'enableLanguageDetection' => false,
//    'enableDefaultLanguageUrlCode' => false,

    'languages' => ['ru', 'uz'],
    'rules' => [
        '/' => 'site/index',

        // 'page/<page_id:\d+>' => 'page/view',

        // 'symbol/<slug:\w+>' => 'symbol/inner',

        // 'site/inner/<id:\d+>' => 'site/inner',

        // 'site/blog/<slug:[\w-]+>' => 'site/blog',

        '<lang:\w+>/<alias:\w+>' => 'site/<alias>',
    ],
];