<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Parkovka';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hero-wrap" style="background-image: url('<?= Yii::getAlias('@web') ?>/deluxe/images/bg_1.jpg');">
  <div class="overlay"></div>
  <div class="container">
    <div class="row no-gutters slider-text d-flex align-itemd-end justify-content-center">
      <div class="col-md-9 ftco-animate text-center d-flex align-items-end justify-content-center">
        <div class="text">
          <p class="breadcrumbs mb-2" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><span class="mr-2"><a href="<?= Url::to(['site/index']) ?>"><?= Yii::t("template", "Bosh sahifa");?></a></span> <span class="mr-2"><a href="<?= Url::to(['site/blog']) ?>"></a></span></p>
          <h1 class="mb-4 bread"><?= Yii::t("template", "Parkovka");?></h1>
        </div>
      </div>
    </div>
  </div>
</div>
<?php if (Yii::$app->language == 'ru') : ?>

  <?php foreach ($parkovkaRus as $parkovkaRu) : ?>
    <section class="ftco-section ftco-degree-bg">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 ftco-animate order-md-last">
            <h2 class="mb-3"><?= $parkovkaRu->name; ?></h2>
            <p><?= $parkovkaRu->title; ?></p>
            <p>
              <img src="<?= Yii::getAlias('@web') ?>/uploads/parking/<?= $parkovkaRu->image; ?>" alt="" class="img-fluid">
            </p>
            <p><?= $parkovkaRu->text; ?></p>

          </div> <!-- .col-md-8 -->
        </div>
      </div>
    </section> <!-- .section -->
  <?php endforeach; ?>
<?php else : ?>
  <?php foreach ($parkovkaUzs as $parkovkaUz) : ?>
    <section class="ftco-section ftco-degree-bg">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 ftco-animate order-md-last">
            <h2 class="mb-3"><?= $parkovkaUz->name; ?></h2>
            <p><?= $parkovkaUz->title; ?></p>
            <p>
              <img src="<?= Yii::getAlias('@web') ?>/uploads/parking/<?= $parkovkaUz->image; ?>" alt="" class="img-fluid">
            </p>
            <p><?= $parkovkaUz->text; ?></p>

          </div> <!-- .col-md-8 -->
        </div>
      </div>
    </section> <!-- .section -->
  <?php endforeach; ?>
<?php endif; ?>