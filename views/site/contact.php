<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;

if (Yii::$app->language == 'ru') {
  $this->title = "Контакт";
} else {
  $this->title = "Contact";
}

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hero-wrap" style="background-image: url('<?= Yii::getAlias('@web') ?>/deluxe/images/bg_1.jpg');">
  <div class="overlay"></div>
  <div class="container">
    <div class="row no-gutters slider-text d-flex align-itemd-end justify-content-center">
      <div class="col-md-9 ftco-animate text-center d-flex align-items-end justify-content-center">
        <div class="text">
          <p class="breadcrumbs mb-2"><span class="mr-2"><a href="<?= Yii::$app->language == 'ru' ? Url::to(['site/index']) : Url::to(['site/uz']) ?>"><?= Yii::t("template", "Bosh sahifa"); ?></a></span></p>
          <h1 class="mb-4 bread"><?= Yii::t("template", "Biz bilan Bog`lanish"); ?></h1>
        </div>
      </div>
    </div>
  </div>
</div>


<section class="ftco-section contact-section bg-light">
  <div class="container">
    <div class="row d-flex mb-5 contact-info">
      <div class="col-md-12 mb-4">
        <h2 class="h3"><?= Yii::t("template", "Bog`lanish uchun ma`lumot"); ?></h2>
      </div>
      <div class="w-100"></div>
      <div class="col-md-3 d-flex">
        <div class="info bg-white p-4">
          <p><span><?= Yii::t("template", "Adress"); ?>:</span>Uzbekistan, Toshkent viloyati, Bostonliq tumani, Humson Shaharchasi</p>
        </div>
      </div>
      <div class="col-md-3 d-flex">
        <div class="info bg-white p-4">
          <p><span><?= Yii::t("template", "Telefon"); ?>:</span> <a href="tel://1234567920">78 1297722 (+99890)3292288</a></p>
        </div>
      </div>
      <div class="col-md-3 d-flex">
        <div class="info bg-white p-4">
          <p><span>Email:</span> <a href="mailto:info@yoursite.com">info@yoursite.com</a></p>
        </div>
      </div>
      <div class="col-md-3 d-flex">
        <div class="info bg-white p-4">
          <p><span><?= Yii::t("template", "Sayt"); ?></span> <a href="#">yoursite.com</a></p>
        </div>
      </div>
    </div>
    <div class="row block-9">
      <div class="col-md-6 order-md-last d-flex">
        <?php $form = ActiveForm::begin(['id' => 'contact-form', 'class' => 'bg-white p-5 contact-form']); ?>
        <div class="form-group">
          <?= $form->field($model, 'name')->textInput(['autofocus' => true, 'placeholder' => 'Ismingiz']) ?>
        </div>
        <div class="form-group">
          <?= $form->field($model, 'email')->textInput(['autofocus' => true, 'placeholder' => 'Emailingiz']) ?>
        </div>
        <div class="form-group">
          <?= $form->field($model, 'subject')->textInput(['autofocus' => true, 'placeholder' => '']) ?>
        </div>
        <div class="form-group">
          <?= $form->field($model, 'body')->textarea(['rows' => 7, 'cols' => 30, 'placeholder' => 'Xabar']) ?>
        </div>
        <div class="form-group">
          <?= Html::submitButton('Submit Form', ['class' => 'btn btn-primary py-3 px-5', 'name' => 'contact-button']) ?>
        </div>
        <?php ActiveForm::end(); ?>
      </div>

      <div class="col-md-6 d-flex">
        <div>
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2978.800327806262!2d69.9371733156777!3d41.70324598431688!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38af3df13d373b9b%3A0xb478d12147813ca3!2z0JfQvtC90LAg0L7RgtC00YvRhdCwINCj0LPQsNC8!5e0!3m2!1sru!2s!4v1592421576778!5m2!1sru!2s" width="550" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
      </div>
    </div>
  </div>
</section>