<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

if (Yii::$app->language == 'ru') {
  $this->title = 'Видео';
} else {
  $this->title = 'Video';
}


$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hero-wrap" style="background-image: url('<?= Yii::getAlias('@web') ?>/deluxe/images/bg_1.jpg');">
  <div class="overlay"></div>
  <div class="container">
    <div class="row no-gutters slider-text d-flex align-itemd-end justify-content-center">
      <div class="col-md-9 ftco-animate text-center d-flex align-items-end justify-content-center">
        <div class="text">
          <p class="breadcrumbs mb-2" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><span class="mr-2"><a href="<?= Url::to(['site/index']) ?>"><?= Yii::t("template", "Bosh sahifa"); ?></a></span> <span class="mr-2"><a href="<?= Url::to(['site/blog']) ?>"></a></span></p>
          <h1 class="mb-4 bread"><?= Yii::t("template", "Video"); ?></h1>
        </div>
      </div>
    </div>
  </div>
</div>
<?php if (Yii::$app->language == 'ru') : ?>

  <?php foreach ($videoRus as $videoRu) : ?>

    <section class="ftco-section ftc-no-pb ftc-no-pt" style="margin-top: 10px; margin-bottom: 10px;">
      <div class="container">
        <div class="row">
          <div class="col-md-5 p-md-5 img img-2 d-flex justify-content-center align-items-center" style="background-image: url(<?= Yii::getAlias('@web') ?>/deluxe/images/bg_2.jpg);">
            <a href="<?= Yii::getAlias('@web') ?>/uploads/video/<?= $videoRu->link; ?>" class="icon popup-vimeo d-flex justify-content-center align-items-center">
              <span class="icon-play"></span>
            </a>
          </div>
          <div class="col-md-7 py-5 wrap-about pb-md-5 ftco-animate">
            <div class="heading-section heading-section-wo-line pt-md-5 pl-md-5 mb-5">
              <div class="ml-md-0">
                <span class="subheading"><?= $videoRu->name; ?></span>
                <h2 class="mb-4"><?= $videoRu->title; ?></h2>
              </div>
            </div>
            <div class="pb-md-5">
              <p><?= $videoRu->text; ?></p>
              <!-- <p>On her way she met a copy. The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word "and" and the Little Blind Text should turn around and return to its own, safe country. But nothing the copy said could convince her and so it didn’t take long until a few insidious Copy Writers ambushed her, made her drunk with Longe and Parole and dragged her into their agency, where they abused her for their.</p> -->
              <!-- <p>When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane. Pityful a rethoric question ran over her cheek, then she continued her way.</p> -->
              <ul class="ftco-social d-flex">
                <li class="ftco-animate"><a href="https://t.me/ugamoromgohi" target="_blank"><span class="icon-telegram"></span></a></li>
                <li class="ftco-animate"><a href="https://www.facebook.com/ugam.oromgohi.3" target="_blank"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="https://instagram.com/ugamoromgohi?r=nametag" target="_blank"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
  <?php endforeach; ?>
<?php else : ?>
  <?php foreach ($videoUzs as $videoUz) : ?>

    <section class="ftco-section ftc-no-pb ftc-no-pt" style="margin-top: 10px; margin-bottom: 10px;">
      <div class="container">
        <div class="row">
          <div class="col-md-5 p-md-5 img img-2 d-flex justify-content-center align-items-center" style="background-image: url(<?= Yii::getAlias('@web') ?>/deluxe/images/bg_2.jpg);">
            <a href="<?= Yii::getAlias('@web') ?>/uploads/video/<?= $videoUz->link; ?>" class="icon popup-vimeo d-flex justify-content-center align-items-center">
              <span class="icon-play"></span>
            </a>
          </div>
          <div class="col-md-7 py-5 wrap-about pb-md-5 ftco-animate">
            <div class="heading-section heading-section-wo-line pt-md-5 pl-md-5 mb-5">
              <div class="ml-md-0">
                <span class="subheading"><?= $videoUz->name; ?></span>
                <h2 class="mb-4"><?= $videoUz->title; ?></h2>
              </div>
            </div>
            <div class="pb-md-5">
              <p><?= $videoUz->text; ?></p>
              <!-- <p>On her way she met a copy. The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word "and" and the Little Blind Text should turn around and return to its own, safe country. But nothing the copy said could convince her and so it didn’t take long until a few insidious Copy Writers ambushed her, made her drunk with Longe and Parole and dragged her into their agency, where they abused her for their.</p> -->
              <!-- <p>When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane. Pityful a rethoric question ran over her cheek, then she continued her way.</p> -->
              <ul class="ftco-social d-flex">
                <li class="ftco-animate"><a href="https://t.me/ugamoromgohi" target="_blank"><span class="icon-telegram"></span></a></li>
                <li class="ftco-animate"><a href="https://www.facebook.com/ugam.oromgohi.3" target="_blank"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="https://instagram.com/ugamoromgohi?r=nametag" target="_blank"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
  <?php endforeach; ?>
<?php endif; ?>