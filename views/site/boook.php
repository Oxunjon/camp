<?php

/* @var $this yii\web\View */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

if (Yii::$app->language == 'ru') {
  $this->title = 'ЗАБРОНИРОВАТЬ';
} else {
  $this->title = "BAND QILISH";
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hero-wrap" style="background-image: url('<?= Yii::getAlias('@web') ?>/deluxe/images/bg_1.jpg');">
  <div class="overlay"></div>
  <div class="container">
    <div class="row no-gutters slider-text d-flex align-itemd-end justify-content-center">
      <div class="col-md-9 ftco-animate text-center d-flex align-items-end justify-content-center">
        <div class="text">
          <p class="breadcrumbs mb-2" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><span class="mr-2"><a href="<?= Url::to(['site/index']) ?>"></a></span> <span class="mr-2"><a href="<?= Url::to(['site/blog']) ?>"></a></span></p>
          <h1 class="mb-4 bread"><?= Yii::$app->language == 'ru' ? $this->title : $this->title; ?></h1>
        </div>
      </div>
    </div>
  </div>
</div>


<section class="ftco-section contact-section bg-light">

  <div class="row block-10" style="display:flex; align-content: center; justify-content: center;">
    <div class="col-md-8 order-md-last">
      <?php $form = ActiveForm::begin(['id' => 'contact-form', 'class' => 'bg-white p-5 contact-form']); ?>
      <div class="form-group">
        <?= $form->field($model, 'name')->textInput(['autofocus' => true, 'placeholder' => Yii::$app->language == 'ru' ? 'Имя' : "Ismingiz", 'style' => 'border-radius: 12px']) ?>
      </div>
      <div class="form-group">
        <?= $form->field($model, 'telephone')->textInput(['autofocus' => true, 'placeholder' => Yii::$app->language == 'ru' ? 'Телефон' : 'Telefon', 'style' => 'border-radius: 12px']) ?>
      </div>
      <div class="form-group">
        <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary py-3 px-5', 'name' => 'contact-button', "id" => 'form-group']) ?>
      </div>
      <?php ActiveForm::end(); ?>
    </div>


  </div>
  </div>
</section>