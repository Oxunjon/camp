<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

if (Yii::$app->language == 'ru') {
  $this->title = 'Детская площадка';
} else {
  $this->title = 'Bolalarmaydoni';
}

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hero-wrap" style="background-image: url('<?= Yii::getAlias('@web') ?>/deluxe/images/bg_1.jpg');">
  <div class="overlay"></div>
  <div class="container">
    <div class="row no-gutters slider-text d-flex align-itemd-end justify-content-center">
      <div class="col-md-9 ftco-animate text-center d-flex align-items-end justify-content-center">
        <div class="text">
          <p class="breadcrumbs mb-2" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><span class="mr-2"><a href="<?= Url::to(['site/index']) ?>"><?= Yii::t("template", "Bosh sahifa"); ?></a></span> <span class="mr-2"><a href="<?= Url::to(['site/blog']) ?>"></a></span></p>
          <h1 class="mb-4 bread"><?= Yii::t("template", "Bolalar maydonchasi"); ?></h1>
        </div>
      </div>
    </div>
  </div>
</div>


<section class="ftco-section">
  <div class="container">
    <div class="row d-flex">
      <?php if (Yii::$app->language == 'ru') : ?>

        <?php foreach ($bolalarmaydoniRus as $bolalarmaydoniRu) : ?>
          <div class="col-md-3 d-flex ftco-animate">
            <div class="align-self-stretch">
              <a href="#" class="block-20" style="background-image: url('<?= Yii::getAlias('@web') ?>/uploads/bolalarmaydoni/<?= $bolalarmaydoniRu->image ?>');">
              </a>
              <div class="text mt-3">
                <h3 class="heading mt-3"><a href="#"><?= $bolalarmaydoniRu->title ?></a></h3>

              </div>
            </div>
          </div>
        <?php endforeach; ?>
      <?php else : ?>
        <?php foreach ($bolalarmaydoniUzs as $bolalarmaydoniUz) : ?>
          <div class="col-md-3 d-flex ftco-animate">
            <div class="align-self-stretch">
              <a href="#" class="block-20" style="background-image: url('<?= Yii::getAlias('@web') ?>/uploads/bolalarmaydoni/<?= $bolalarmaydoniUz->image ?>');">
              </a>
              <div class="text mt-3">
                <h3 class="heading mt-3"><a href="#"><?= $bolalarmaydoniUz->title ?></a></h3>

              </div>
            </div>
          </div>
        <?php endforeach; ?>
      <?php endif; ?>

    </div>
</section>