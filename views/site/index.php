<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;
/*@var $model app\models\Video*/

if (Yii::$app->language == 'ru') {
  $this->title = 'Курортный комплекс "УГАМ"';
} else {
  $this->title = 'Ugam Oromgohi sanatoriyasi';
}
$this->params['breadcrumbs'][] = $this->title;

?>
<section class="home-slider owl-carousel" data-interval:3000>
  <div class="slider-item" style="background-image:url(<?= Yii::getAlias('@web') ?>/deluxe/images/bg_1.jpg);">
    <div class="overlay"></div>
    <div class="container">
      <div class="row no-gutters slider-text align-items-center justify-content-center">
        <div class="col-md-12 ftco-animate text-center">
          <div class="text mb-5 pb-3">
            <h1 class="mb-3"><?= Yii::$app->language == 'ru' ? 'Добро пожаловать в Курортный комплекс "УГАМ"' : 'Ugam oromgohi sanatoriyasiga xush kelibsiz' ?></h1>
            <h2 style="font-weight: bold;"><?= Yii::$app->language == 'ru' ? "Коттеджи &amp; Комнаты" : "Kottejlar &amp; Xonalar" ?></h2>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="slider-item" style="background-image:url(<?= Yii::getAlias('@web') ?>/deluxe/images/ugam.jpg);">
    <div class="overlay"></div>
    <div class="container">
      <div class="row no-gutters slider-text align-items-center justify-content-center">
        <div class="col-md-12 ftco-animate text-center">
          <div class="text mb-5 pb-3">
            <h1 class="mb-3"><?= Yii::$app->language == 'ru' ? 'Отличное место для отдыха' : "Siz uchun ajoyib dam olish maskani" ?></h1>
            <h2 style="font-weight: bold;"><?= Yii::$app->language == 'ru' ? "Приходите будьте нашим гостем, будем рады вам" : "Tashrif buyuring" ?></h2>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="slider-item" style="background-image:url(<?= Yii::getAlias('@web') ?>/deluxe/images/bg_4.jpg);">
    <div class="overlay"></div>
    <div class="container">
      <div class="row no-gutters slider-text align-items-center justify-content-center">
        <div class="col-md-12 ftco-animate text-center">
          <div class="text mb-5 pb-3">
            <h1 class="mb-3"><?= Yii::$app->language == 'ru' ? 'Отличное место для отдыха' : "Siz uchun ajoyib dam olish maskani" ?></h1>
            <h2 style="font-weight: bold;"><?= Yii::$app->language == 'ru' ? "Приходите будьте нашим гостем, будем рады вам" : "Tashrif buyuring" ?></h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="slider-item" style="background-image:url(<?= Yii::getAlias('@web') ?>/deluxe/images/6494.jpg);">
    <div class="overlay"></div>
    <div class="container">
      <div class="row no-gutters slider-text align-items-center justify-content-center">
        <div class="col-md-12 ftco-animate text-center">
          <div class="text mb-5 pb-3">
            <h1 class="mb-3"><?= Yii::$app->language == 'ru' ? 'Отличное место для отдыха' : "Siz uchun ajoyib dam olish maskani" ?></h1>
            <h2 style="font-weight: bold;"><?= Yii::$app->language == 'ru' ? "Приходите будьте нашим гостем, будем рады вам" : "Tashrif buyuring" ?></h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="slider-item" style="background-image:url(<?= Yii::getAlias('@web') ?>/deluxe/images/bg_5.jpg);">
    <div class="overlay"></div>
    <div class="container">
      <div class="row no-gutters slider-text align-items-center justify-content-center">
        <div class="col-md-12 ftco-animate text-center">
          <div class="text mb-5 pb-3">
            <h1 class="mb-3"><?= Yii::$app->language == 'ru' ? 'Отличное место для отдыха' : "Siz uchun ajoyib dam olish maskani" ?></h1>
            <h2 style="font-weight: bold;"><?= Yii::$app->language == 'ru' ? "Приходите будьте нашим гостем, будем рады вам" : "Tashrif buyuring" ?></h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="slider-item" style="background-image:url(<?= Yii::getAlias('@web') ?>/deluxe/images/bg_3.jpg);">
    <div class="overlay"></div>
    <div class="container">
      <div class="row no-gutters slider-text align-items-center justify-content-center">
        <div class="col-md-12 ftco-animate text-center">
          <div class="text mb-5 pb-3">
            <h1 class="mb-3"><?= Yii::$app->language == 'ru' ? 'Отличное место для отдыха' : "Siz uchun ajoyib dam olish maskani" ?></h1>
            <h2 style="font-weight: bold;"><?= Yii::$app->language == 'ru' ? "Приходите будьте нашим гостем, будем рады вам" : "Tashrif buyuring" ?></h2>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="ftco-booking">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <form action="<?= Yii::$app->language == 'ru' ? Url::to(['boook']) : Url::to(['uz-boook']); ?>" class="booking-form">
          <div class="row">



            <div class="col-md d-flex">
              <div class="form-group d-flex align-self-stretch">
                <input type="submit" style="font-weight: 900;" value="<?= Yii::$app->language == 'ru' ? "ЗАБРОНИРОВАТЬ" : "BAND QILISH"; ?>" <?= Yii::$app->language == 'ru' ? "ЗАБРОНИРОВАТЬ" : "BAND QILISH"; ?> class="btn btn-primary py-3 px-4 align-self-stretch">
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>



<section class="ftco-section ftc-no-pb ftc-no-pt">
  <div class="container">
    <div class="row">

      <div class="col-md-5 p-md-5 img img-2 d-flex justify-content-center align-items-center" style="background-image: url(<?= Yii::getAlias('@web') ?>/deluxe/images/bg_2.jpg);">
        <a href="<?= $videos->link; ?>" class="icon popup-vimeo d-flex justify-content-center align-items-center">
          <span class="icon-play"></span>
        </a>
      </div>
      <div class="col-md-7 py-5 wrap-about pb-md-5 ftco-animate">
        <div class="heading-section heading-section-wo-line pt-md-5 pl-md-5 mb-5">
          <div class="ml-md-0">
            <span class="subheading"><?= $videos->name; ?></span>
            <h2 class="mb-4"><?= $videos->title; ?></h2>
          </div>
        </div>
        <div class="pb-md-5">
          <p><?= $videos->text; ?></p>
          <!-- <p>On her way she met a copy. The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word "and" and the Little Blind Text should turn around and return to its own, safe country. But nothing the copy said could convince her and so it didn’t take long until a few insidious Copy Writers ambushed her, made her drunk with Longe and Parole and dragged her into their agency, where they abused her for their.</p> -->
          <!-- <p>When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane. Pityful a rethoric question ran over her cheek, then she continued her way.</p> -->
          <ul class="ftco-social d-flex">
            <li class="ftco-animate"><a href="https://t.me/ugamoromgohi" target="_blank"><span class="icon-telegram"></span></a></li>
            <li class="ftco-animate"><a href="https://www.facebook.com/ugam.oromgohi.3" target="_blank"><span class="icon-facebook"></span></a></li>
            <li class="ftco-animate"><a href="https://instagram.com/ugamoromgohi?r=nametag" target="_blank"><span class="icon-instagram"></span></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>



<section class="ftco-section">
  <div class="container">
    <div class="row d-flex">
      <div class="col-md-3 d-flex align-self-stretch ftco-animate">
        <div class="media block-6 services py-4 d-block text-center">
          <div class="d-flex justify-content-center">
            <div class="icon d-flex align-items-center justify-content-center">
              <span class="flaticon-reception-bell"></span>
            </div>
          </div>
          <div class="media-body p-2 mt-2">
            <h3 class="heading mb-3">
              <a class="label-a" href=<?= Yii::$app->language == 'ru' ? Url::to(['site/tibbiyot']) : Url::to(['site/uz-tibbiyot']) ?>><?= Yii::$app->language == 'ru' ? "Медицинский центр" : "Tibbiyot-Markazi" ?></a>
            </h3>
            <p>Уникальный, многопрофильный медицинский центр с широким спектром амбулаторно-поликлинических услуг</p>
          </div>
        </div>
      </div>
      <div class="col-md-3 d-flex align-self-stretch ftco-animate">
        <div class="media block-6 services py-4 d-block text-center">
          <div class="d-flex justify-content-center">
            <div class="icon d-flex align-items-center justify-content-center">
              <span class="flaticon-serving-dish"></span>
            </div>
          </div>
          <div class="media-body p-2 mt-2">
            <h3 class="heading mb-3">
              <a class="label-a" href="<?= Yii::$app->language == 'ru' ? Url::to(['site/restaurant']) : Url::to(['site/uz-restaurant']) ?>"><?= Yii::$app->language == 'ru' ? "Ресторан" : "Restoran" ?></a>
            </h3>
            <p>Уютный ресторан с узбекской и европейской кухнями</p>
          </div>
        </div>
      </div>
      <div class="col-md-3 d-flex align-sel Searchf-stretch ftco-animate">
        <div class="media block-6 services py-4 d-block text-center">
          <div class="d-flex justify-content-center">
            <div class="icon d-flex align-items-center justify-content-center">
              <span class="flaticon-car"></span>
            </div>
          </div>
          <div class="media-body p-2 mt-2">
            <h3 class="heading mb-3">
              <a class="label-a" href="<?= Yii::$app->language == 'ru' ? Url::to(['site/parkovka']) : Url::to(['site/uz-parkovka']) ?>"><?= Yii::$app->language == 'ru' ?  "Парковка" : "Parkovka" ?></a>
            </h3>
            <p>Лучшее место для парковки</p>
          </div>
        </div>
      </div>
      <div class="col-md-3 d-flex align-self-stretch ftco-animate">
        <div class="media block-6 services py-4 d-block text-center">
          <div class="d-flex justify-content-center">
            <div class="icon d-flex align-items-center justify-content-center">
              <span class="flaticon-spa"></span>
            </div>
          </div>
          <div class="media-body p-2 mt-2">
            <h3 class="heading mb-3">
              <a class="label-a" href="<?= Yii::$app->language == 'ru' ? Url::to(['site/xizmat-basseyn']) : Url::to(['site/uz-basseyn']) ?>"><?= Yii::$app->language == 'ru' ?  "Бассейн" : "Basseyn" ?></a>
            </h3>
            <p> это прекрасное место для отдыха с удобными современными шезлонгами</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="ftco-section bg-light">
  <div class="container">
    <div class="row justify-content-center mb-5 pb-3">
      <div class="col-md-7 heading-section text-center ftco-animate">
        <h2 class="mb-4"><?= Yii::t("template", "Bizning xonalar"); ?></h2>
      </div>
    </div>
    <div class="row">
    
      <?php foreach ($roomRus as $roomRu) : ?>
        <div class="col-sm col-md-6 col-lg-4 ftco-animate">

          <div class="room">

            <a href="<?= $roomRu->small_text == '<p>Люкс</p>' ? Url::to(['site/lyuksxona']) : Url::to(['site/vipxona']) ?>" class="img d-flex justify-content-center align-items-center"\ style="background-image: url(<?= Yii::getAlias('@web') ?>/uploads/room/<?= $roomRu->image; ?>);">
              <div class="icon d-flex justify-content-center align-items-center">
                <span class="icon-search2"></span>
              </div>
            </a>
            <div class="text p-3 text-center">
              <h3 class="mb-3"><a href="<?= $roomRu->small_text == '<p>Люкс</p>' ? Url::to(['site/lyuksxona']) : Url::to(['site/vipxona']) ?>"><?= $roomRu->small_text ?></a></h3>
              <p><span class="price mr-2"><?= $roomRu->price; ?>$</span> <span class="per">за ночь</span></p>
              <hr>
              <p class="pt-1"><a href="<?= $roomRu->small_text == '<p>Люкс</p>' ? Url::to(['site/lyuksxona']) : Url::to(['site/vipxona']) ?>" class="btn-custom">Подробнее <span class="icon-long-arrow-right"></span></a></p>
            </div>

          </div>

        </div>
      <?php endforeach ?>
    </div>
  </div>
</section>


<section class="ftco-section ftco-counter img" id="section-counter" style="background-image: url(<?= Yii::getAlias('@web') ?>/deluxe/images/bg_1.jpg);">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-10">
        <div class="row">
          <div class="col-md-4 d-flex justify-content-center counter-wrap ftco-animate">
            <div class="block-18 text-center">
              <div class="text">
                <strong class="number" data-number="350">0</strong>
                <span style="font-weight: 900; border-radius:70%;"><?= Yii::$app->language == 'ru' ? 'Вместимость' : "Mehmonlar Sig'imi" ?></span>
              </div>
            </div>
          </div>
          <div class="col-md-4 d-flex justify-content-center counter-wrap ftco-animate">
            <div class="block-18 text-center">
              <div class="text">
                <div>
                  <strong class="number" data-number="21">0</strong>
                  <span style="font-weight: 900; "><?= Yii::$app->language == 'ru' ? "Коттеджи" : "Kottejlar" ?></span>
                </div>

                <div class="left-right" style="padding-top: 2vh;">
                  <span style="padding-right: 10vh;">VIP</span>
                  <span style="padding-left: 10vh; margin-top: -1.7em;  "><?= Yii::$app->language == 'ru' ? "Коттедж" : "Kottej" ?></span>
                  <strong class="number" data-number="4" style="margin-right: 10vh;"></strong>
                  <strong class="number" data-number="17" style="margin-left: -2vh;">0</strong>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 d-flex justify-content-center counter-wrap ftco-animate">
            <div class="block-18 text-center">
              <div class="text">
                <div>
                  <strong class="number" data-number="75">0</strong>
                  <span style="font-weight: 900; border-radius:70%;"><?= Yii::$app->language == 'ru' ? "Комнаты" : "Xonalar" ?></span>
                </div>

                <div class="left-right" style="padding-top: 2vh;">
                  <span style="padding-right: 10vh;"><?= Yii::$app->language == "ru" ? "Полюкс" : "Polyuks"; ?></span>
                  <span style="padding-left: 10vh; margin-top: -1.7em; "><?= Yii::$app->language == "ru" ? "Люкс" : "Lyuks"; ?></span>
                  <strong class="number" data-number="35" style="margin-right: 10vh;">0</strong>
                  <strong class="number" data-number="40" style="margin-left: -2vh;">0</strong>
                </div>
              </div>
            </div>
          </div>
          <!-- <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
            <div class="block-18 text-center">
              <div class="text">
                <strong class="number" data-number="100">0</strong>
                <span>Destination</span>
              </div>
            </div>
          </div> -->
        </div>
      </div>
    </div>
  </div>
</section>






<div class="row no-gutters justify-content-center pb-5">
  <div class="col-md-7 text-center heading-section ftco-animate">
    <h2><span><?= Yii::t("template", "Galereya") ?></span></h2>
  </div>
</div>


<section class="instagram pt-5 home-slider owl-carousel" style="display:flex; justify-content: space-between !important;">
  <?php foreach ($gallerys as $gallery) : ?>
    <div class="container-fluid">

      <div class="row no-gutters">

        <div class="col-sm-12 col-md">
          <a href="<?= Yii::getAlias('@web') ?>/uploads/gallery/<?= $gallery->parking_image; ?>" class="insta-img image-popup" style=" background-image: url(<?= Yii::getAlias('@web') ?>/uploads/gallery/<?= $gallery->parking_image; ?>);"></a>
        </div>
        <div class="col-sm-12 col-md">
          <a href="<?= Yii::getAlias('@web') ?>/uploads/gallery/<?= $gallery->kottej_image; ?>" class="insta-img image-popup" style="background-image: url(<?= Yii::getAlias('@web') ?>/uploads/gallery/<?= $gallery->kottej_image; ?>);"></a>
        </div>
        <div class="col-sm-12 col-md">
          <a href="<?= Yii::getAlias('@web') ?>/uploads/gallery/<?= $gallery->blog_image; ?>" class="insta-img image-popup" style="background-image: url(<?= Yii::getAlias('@web') ?>/uploads/gallery/<?= $gallery->blog_image; ?>);"></a>
        </div>


      </div>
    </div>
  <?php endforeach; ?>
</section>