<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

if (Yii::$app->language == 'ru') {
  $this->title = 'Услуги';
} else {
  $this->title = 'Xizmatlar';
}
// $this->title = 'Xizmat';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hero-wrap" style="background-image: url('<?= Yii::getAlias('@web') ?>/deluxe/images/bg_1.jpg');">
  <div class="overlay"></div>
  <div class="container">
    <div class="row no-gutters slider-text d-flex align-itemd-end justify-content-center">
      <div class="col-md-9 ftco-animate text-center d-flex align-items-end justify-content-center">
        <div class="text">
          <p class="breadcrumbs mb-2" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><span class="mr-2"><a href="<?= Yii::$app->language == 'ru' ? Url::to(['site/index']) : Url::to(['site/uz']) ?>"><?= Yii::t("template", "Bosh sahifa") ?></a></span></p>
          <h1 class="mb-4 bread"><?= Yii::t("template", "Xizmatlar") ?></h1>
        </div>
      </div>
    </div>
  </div>
</div>

<section class="ftco-section ftco-degree-bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 ftco-animate order-md-last">
        <h2 class="mb-3"><?= $park->name ?></h2>
        <p><?= $park->title ?></p>
        <p>
          <img src="<?= Yii::getAlias('@web') ?>/uploads/parking/<?= $park->image ?>" alt="" class="img-fluid">
        </p>
        <p><?= $park->text ?></p>
        <h2 class="mb-3 mt-5">#2. <?= $second->name ?></h2>
        <p><?= $second->title ?></p>
        <p>
          <img src="<?= Yii::getAlias('@web') ?>/uploads/parking/<?= $second->image ?>" alt="" class="img-fluid">
        </p>
        <p><?= $second->text ?></p>




      </div> <!-- .col-md-8 -->
      <div class="col-lg-4 sidebar ftco-animate">

        <div class="sidebar-box ftco-animate">
          <div class="categories">
            <h3><?= Yii::t("template", "Xizmatlar"); ?></h3>
            <li><a href="<?= Yii::$app->language == 'ru' ? Url::to(['xizmat-besedka']) : Url::to(['uz-besedka']); ?>"><?= Yii::t("template", "Besedka"); ?> </a></li>
            <li><a href="<?= Yii::$app->language == 'ru' ? Url::to(['xizmat-sauna']) : Url::to(['uz-sauna']); ?>"><?= Yii::t("template", "Sauna"); ?> </a></li>
            <li><a href="<?= Yii::$app->language == 'ru' ? Url::to(['xizmat-restaurant']) : Url::to(['uz-restaurant']); ?>"><?= Yii::t("template", "Oshxona"); ?> </a></li>
            <li><a href="<?= Yii::$app->language == 'ru' ? Url::to(['xizmat-basseyn']) : Url::to(['uz-basseyn']); ?>"><?= Yii::t("template", "Basseyn"); ?> </a></li>
            <li><a href="<?= Yii::$app->language == 'ru' ? Url::to(['xizmat-raqsmaydoni']) : Url::to(['uz-raqsmaydoni']); ?>"><?= Yii::t("template", "Raqs maydoni"); ?></a></li>
            <li><a href="<?= Yii::$app->language == 'ru' ? Url::to(['xizmat-bolalarmaydoni']) : Url::to(['uz-bolalarmaydoni']); ?>"><?= Yii::t("template", "Bolalar maydonchasi"); ?> </a></li>
            <li><a href="<?= Yii::$app->language == 'ru' ? Url::to(['xizmat-jonliburchak']) : Url::to(['uz-jonliburchak']); ?>"><?= Yii::t("template", "Jonli burchak"); ?></a></li>
          </div>
        </div>


      </div>

    </div>
  </div>
</section> <!-- .section -->