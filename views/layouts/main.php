<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

if (Yii::$app->language == 'ru') {
  $this->title = 'Курортный комплекс "УГАМ"';
}
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="ugamoromgohi, ugam, oromgoh, basseyn, sauna, kompleks, dam olish maskani">
<?php $this->registerCsrfMetaTags() ?>
<title><?= Html::encode($this->title) ?></title>
<?php $this->head() ?>
<link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
  <?php $this->beginBody() ?>

  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
      <a class="navbar-brand" href="<?= Yii::$app->language == 'ru' ? Url::to(['site/index']) : Url::to(['site/uz']) ?>"><img src="">Ugam</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="oi oi-menu"></span> Menu
      </button>

      <div class="collapse navbar-collapse" id="ftco-nav">

        <nav>
          <ul class="navbar-nav ml-auto">

            <li class="nav-item"><a href="<?= Yii::$app->language == 'ru' ? Url::to(['site/index']) : Url::to(['site/uz']) ?>" class="nav-link"><?= Yii::t("template", 'Bosh sahifa') ?></a></li>
            <li class="nav-item"><a href="<?= Yii::$app->language == 'ru' ? Url::to(['site/tibbiyot']) : Url::to(['site/uz-tibbiyot']) ?>" class="nav-link"><?= Yii::t("template", 'Tibbiyot markazi') ?></a></li>

            <li class="nav-item"><a href="<?= Yii::$app->language == "ru" ?  Url::to(['site/about']) : Url::to(['site/about/rus']) ?>"></a>

              <a title="Kottejlar" data-toggle="dropdown" class="nav-link dropdown-toggle" aria-expanded="true"><?= Yii::t("template", "Kottejlar") ?><span class="caret"></span></a>
              <ul role="menu" class=" dropdown-menu">
                <li class="nav-item">
                  <a href="<?= Yii::$app->language == 'ru' ? Url::to(['site/vipkottej']) : Url::to(['site/uz-vipkottej']) ?>" class="nav-link"><?= Yii::t("template", "Vip kottej") ?></a>
                </li>
                <li class="nav-item">
                  <a href="<?= Yii::$app->language == 'ru' ? Url::to(['site/lyukskottej']) : Url::to(['site/uz-lyukskottej']) ?>" class="nav-link"><?= Yii::t("template", "Lyuks kottej 10x") ?></a>
                </li>
                <li class="nav-item">
                  <a href="<?= Yii::$app->language == 'ru' ? Url::to(['site/kottej4x']) : Url::to(['site/uz-kottej4x']) ?>" class="nav-link"><?= Yii::t("template", "Kottej 4x") ?></a>
                </li>
                <li class="nav-item">
                  <a href="<?= Yii::$app->language == 'ru' ? Url::to(['site/kottej5x']) : Url::to(['site/uz-kottej5x']) ?>" class="nav-link"><?= Yii::t("template", "Kottej 5x") ?></a>
                </li>
                <li class="nav-item">
                  <a href="<?= Yii::$app->language == 'ru' ? Url::to(['site/kottej6x']) : Url::to(['site/uz-kottej6x']) ?>" class="nav-link"><?= Yii::t("template", "Kottej 6x") ?></a>
                </li>
                <li class="nav-item">
                  <a href="<?= Yii::$app->language == 'ru' ? Url::to(['site/kottej7x']) : Url::to(['site/uz-kottej7x']) ?>" class="nav-link"><?= Yii::t("template", "Kottej 7x") ?></a>
                </li>
                <li class="nav-item">
                  <a href="<?= Yii::$app->language == 'ru' ? Url::to(['site/kottej8x']) : Url::to(['site/uz-kottej8x']) ?>" class="nav-link"><?= Yii::t("template", "Kottej 8x") ?></a>
                </li>
              </ul>
            </li>

            <li class="nav-item"><a href="<?= Yii::$app->language == "ru" ?  Url::to(['site/about']) : Url::to(['site/about/rus']) ?>"></a>

              <a title="Kottejlar" data-toggle="dropdown" class="nav-link dropdown-toggle" aria-expanded="true"><?= Yii::t("template", "Xonalar") ?><span class="caret"></span></a>
              <ul role="menu" class=" dropdown-menu">
                <li class="nav-item">
                  <a href="<?= Yii::$app->language == 'ru' ? Url::to(['site/vipxona']) : Url::to(['site/uz-vipxona']); ?>" class="nav-link"><?= Yii::t("template", 'Polyuks xona') ?></a>
                </li>
                <li class="nav-item">
                  <a href="<?= Yii::$app->language == 'ru' ? Url::to(['site/lyuksxona']) : Url::to(['site/uz-lyuksxona']); ?>" class="nav-link"><?= Yii::t("template", "Lyuks xona") ?></a>
                </li>

              </ul>
            </li>


            <li class="nav-item"><a href="<?= Yii::$app->language == 'ru' ? Url::to(['site/xizmat']) : Url::to(['site/uz-xizmat']); ?>" class="nav-link"><?= Yii::t("template", "Xizmatlar") ?></a></li>
            <li class="nav-item"><a href="<?= Yii::$app->language == 'ru' ? Url::to(['site/contact']) : Url::to(['site/uz-contact']); ?>" class="nav-link"><?= Yii::t("template", "Bog`lanish") ?></a></li>
            <li class="nav-item"><a href="<?= Yii::$app->language == 'ru' ? Url::to(['site/video']) : Url::to(['site/uz-video']); ?>" class="nav-link"><?= Yii::t("template", "Video") ?></a></li>
          </ul>
        </nav>


        <nav class="navbar-left">

          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <li class="nav-item">
                <a href="<?= Url::base(true) . '/site/uz'; ?>" class="nav-link">UZ</a>
              </li>
              <li class="nav-item">
                <a href="<?= Url::base(true) . '/site/index'; ?>" class="nav-link">RU</a>
              </li>
            </ul>

          </nav>
          <div class="navbar-dark" style="padding-left: 5vh;">

          </div>
        </div>
      </nav>

    </div>


    <!-- END nav -->




    <?= $content ?>


    <footer class="ftco-footer ftco-bg-dark ftco-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md">
            <div class="ftco-footer-widget mb-5">
            <!-- <h2 class="ftco-heading-2">Deluxe Hotel</h2>
              <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p> -->
              <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                <li class="ftco-animate"><a href="https://t.me/ugamoromgohi" target="_blank"><span class="icon-telegram"></span></a></li>
                <li class="ftco-animate"><a href="https://instagram.com/ugamoromgohi?r=nametag" target="_blank"><span class="icon-instagram"></span></a></li>
                <li class="ftco-animate"><a href="https://www.facebook.com/ugam.oromgohi.3" target="_blank"><span class="icon-facebook"></span></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4 ml-md-5">
              <h2 class="ftco-heading-2"><?= Yii::$app->language == 'ru' ? "Полезные ссылки" : "Foydali Malumotlar" ?></h2>
              <ul class="list-unstyled">
                <li><a href="<?= Url::to(['site/tibbiyot']) ?>" class="py-2 d-block"><?= Yii::t("template", "Tibbiyot markazi"); ?></a></li>
                <li><a href="<?= Yii::$app->language == 'ru' ? Url::to(['site/vipkottej']) : Url::to(['site/uz-vipkottej']) ?>" class="py-2 d-block"><?= Yii::t("template", "Vip kottej"); ?></a></li>
                <li><a href="<?= Yii::$app->language == 'ru' ? Url::to(['site/lyukskottej']) : Url::to(['site/uz-lyukskottej']) ?>" class="py-2 d-block"><?= Yii::t("template", "Lyuks kottej"); ?></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2"><?=Yii::t("template","Maxfiylik"); ?></h2>
              <ul class="list-unstyled">
                <li><a href="#" class="py-2 d-block"><?= Yii::t("template", "Biz haqimizda"); ?></a></li>
                <li><a href="<?= Url::to(['site/contact']) ?>" class="py-2 d-block"><?= Yii::t("template", "Bog`lanish"); ?></a></li>
                <li><a href="<?= Url::to(['site/xizmat']) ?>" class="py-2 d-block"><?= Yii::t("template", "Xizmatlar") ?></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2"><?= Yii::$app->language == 'ru' ? "Наш местоположение : " : "Manzil : " ?></h2>
              <div class="block-23 mb-3">
                <ul>
                  <li><span class="icon icon-map-marker"></span><span class="text"><?= Yii::t("template", "Uzbekistan, Toshkent viloyati, Bostonliq tumani, Humson Shaharchasi"); ?></span></li>


                  <li><span class="icon icon-phone"></span><span class="text"> 78 1297722 (+99890)3292288</span></li>
                  <li><span class="icon icon-envelope"></span><span class="text">info@yourdomain.com</span></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      <!-- <div class="row">
        <div class="col-md-12 text-center">

          <p>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            <!-- Copyright &copy; -->
      <!-- <script>
              document.write(new Date().getFullYear());
            </script> -->
            <!-- All rights reserved | This template is made with <i class="icon-heart color-danger" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a> -->
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
          </p>
        </div>
      </div>
    </div>
  </footer>



  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
    <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
    <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00" /></svg></div>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
    <?php $this->endBody() ?>
  </body>

  </html>
  <?php $this->endPage() ?>