<?php

namespace app\controllers;

use common\models\ListGmail;
use Yii;
use common\models\Question;
use yii\web\Controller;
use yii\filters\VerbFilter;
use common\models\QueueMail;

/**
 * QuestionController implements the CRUD actions for Question model.
 */
class QuestionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    public function actionCreate()
    {
        $model = new Question();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $this->sendTelegramBot($model->name . "\n" . $model->phone_or_telegram);

            Yii::$app->queue->delay(10)->push(new QueueMail([
                'name' => $model->name,
                'phone_or_telegram' => $model->phone_or_telegram,
            ]));
            return $this->goHome();
        }
        return $this->goHome();
    }

    private function sendTelegramBot($message){
        $method = "sendMessage";
        $chatId = ListGmail::find()->where(['status' => 1])->all();
        foreach ($chatId as $item) {
            $data = [];
            $data['chat_id'] = $item->telegram_chat_id;
            $data['text'] = $message;
            $url = "https://api.telegram.org/1138781785:AAGfXBmkamP_T1LUSqE4J-HlLTLb1EPc__8/" . $method;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            $res = curl_exec($ch);
        }

    }

}
