<?php

namespace app\controllers;

use app\models\Basseyn;
use app\models\Besedka;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Meal;
use app\models\Blog;
use app\models\Bolalarmaydoni;
use app\models\Restaurant;
use app\models\Room;
use app\models\Gallery;
use app\models\Jonliburchak;
use app\models\Video;
use app\models\Parking;
use app\models\Raqsmaydoni;
use app\models\Sauna;
use app\models\Boook;
use app\models\Kottej4x;
use app\models\Kottej5x;
use app\models\Kottej6x;
use app\models\Kottej7x;
use app\models\Kottej8x;
use app\models\Lyuks;
use app\models\Lyukskottej;
use app\models\Vip;
use app\models\Vipkottej;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public $enableCsrfValidation = false;
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        $videos = Video::find()->one();
        $gallery = Gallery::find()->all();
        $roomRu = Room::find()->where(['lang' => 'ru'])->all();
        return $this->render('index',  [
            'videos' => $videos,
            'gallerys' => $gallery,
            'roomRus' => $roomRu
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        $video = Video::find()->limit(1)->one();
        return $this->render(
            'about',
            [
                'video' => $video
            ]
        );
    }

    public function actionXizmatRestaurant()
    {
        $image = Restaurant::find()->limit(1)->one();
        $gallery = Gallery::find()->all();
        $meals = Meal::find()->all();
        $mms = Meal::find()->orderBy(['id' => SORT_DESC])->all();
        return $this->render('restaurant', ['meals' => $meals, 'mms' => $mms, 'image' => $image, 'gallerys' => $gallery]);
    }
    public function actionTibbiyot()
    {
        // $blogUz = Blog::find()->where(['lang' => 'uz'])->all();
        $blogRu = Blog::find()->where(['lang' => 'ru'])->all();
        // $blogs = Blog::find()->all();


        return $this->render('tibbiyot', [
            // 'blogUzs' => $blogUz,
            'blogRus' => $blogRu
        ]);

        // return $this->render('blog', ['blogs' => $blogs]);
    }

    public function actionBlogSingle()
    {
        return $this->render('blog-single');
    }

    public function actionRoomSingle()
    {
        // $rooms = Room::find()->all();
        $gallery = Gallery::find()->all();

        return $this->render("room-single", [
            'gallerys' => $gallery
        ]);
    }

    public function actionXizmat()
    {
        $park = Parking::find()->limit(1)->one();
        $second = Parking::find()->orderBy(['id' => SORT_DESC])->one();
        // print_r($second);exit;
        return $this->render(
            'Xizmat',
            [
                'park' => $park,
                'second' => $second
            ]
        );
    }
    public function actionRoom()
    {
        $roomRu = Room::find()->where(['lang' => 'ru'])->all();
        // $rooms = Room::find()->all();
        return $this->render(
            'room',
            [
                'roomRus' => $roomRu
            ]
        );
    }
    public function actionUzRoom()
    {
        $roomRu = Room::find()->where(['lang' => 'ru'])->all();
        $roomUz = Room::find()->where(['lang' => 'uz'])->all();
        return $this->render(
            'room',
            [
                'roomUzs' => $roomUz,
                'roomRus' => $roomRu
            ]
        );
    }



    public function actionParkovka()
    {
        $parkovkaUz = Parking::find()->where(['lang' => 'uz'])->all();
        $parkovkaRu = Parking::find()->where(['lang' => 'ru'])->all();
        // $parkovka = Parking::find()->all();
        return $this->render(
            'parkovka',
            [
                'parkovkaUzs' => $parkovkaUz,
                'parkovkaRus' => $parkovkaRu
            ]
        );
    }

    public function actionXizmatBasseyn()
    {
        // $basseyn = Basseyn::find()->all();
        $basseynUz = Basseyn::find()->where(['lang' => 'uz'])->all();
        $basseynRu = Basseyn::find()->where(['lang' => 'ru'])->all();

        return $this->render(
            'basseyn',
            [
                'basseynUzs' => $basseynUz,
                'basseynRus' => $basseynRu
            ]
        );
    }

    public function actionXizmatSauna()
    {
        // $sauna = Sauna::find()->all();
        $saunaUz = Sauna::find()->where(['lang' => 'uz'])->all();
        $saunaRu = Sauna::find()->where(['lang' => 'ru'])->all();
        return $this->render(
            'sauna',
            [
                'saunasUzs' => $saunaUz,
                'saunaRus' => $saunaRu
            ]
        );
    }

    public function actionXizmatBesedka()
    {
        $besedkaUz = Besedka::find()->where(['lang' => 'uz'])->all();
        $besedkaRu = Besedka::find()->where(['lang' => 'ru'])->all();

        // $besedka = Besedka::find()->all();
        return $this->render(
            'besedka',
            [
                'besedkaUzs' => $besedkaUz,
                'besedkaRus' => $besedkaRu
            ]
        );
    }

    public function actionXizmatRaqsmaydoni()
    {
        $raqsmaydoniUz = Raqsmaydoni::find()->where(['lang' => 'uz'])->all();
        $raqsmaydoniRu = Raqsmaydoni::find()->where(['lang' => 'ru'])->all();
        // $raqsmaydoni = Raqsmaydoni::find()->all();
        return $this->render(
            'raqsmaydoni',
            [
                'raqsmaydoniUzs' => $raqsmaydoniUz,
                'raqsmaydoniRus' => $raqsmaydoniRu
            ]
        );
    }

    public function actionXizmatBolalarmaydoni()
    {
        $bolalarmaydoniUz = Bolalarmaydoni::find()->where(['lang' => 'uz'])->all();
        $bolalarmaydoniRu = Bolalarmaydoni::find()->where(['lang' => 'ru'])->all();
        // $bolalarmaydoni = Bolalarmaydoni::find()->all();
        return $this->render(
            'bolalarmaydoni',
            [
                'bolalarmaydoniUzs' => $bolalarmaydoniUz,
                'bolalarmaydoniRus' => $bolalarmaydoniRu
            ]
        );
    }

    public function actionXizmatJonliburchak()
    {
        $jonliburchakUz = Jonliburchak::find()->where(['lang' => 'uz'])->all();
        $jonliburchakRu = Jonliburchak::find()->where(['lang' => 'ru'])->all();
        // $jonliburchak = Jonliburchak::find()->all();
        return $this->render(
            'jonliburchak',
            [
                'jonliburchakUzs' => $jonliburchakUz,
                'jonliburchakRus' => $jonliburchakRu
            ]
        );
    }

    public function actionVipxona()
    {
        $vipxonaUz = Vip::find()->where(['lang' => 'uz'])->all();
        $vipxonaRu = Vip::find()->where(['lang' => 'ru'])->all();
        // $vipxona = Vip::find()->all();
        return $this->render(
            'vipxona',
            [
                'vipxonaUzs' => $vipxonaUz,
                'vipxonaRus' => $vipxonaRu
            ]
        );
    }

    public function actionLyuksxona()
    {
        $lyuksxonaUz = Lyuks::find()->where(['lang' => 'uz'])->all();
        $lyuksxonaRu = Lyuks::find()->where(['lang' => 'ru'])->all();
        // $lyuksxona = Lyuks::find()->all();
        return $this->render(
            'lyuksxona',
            [
                'lyuksxonaUzs' => $lyuksxonaUz,
                'lyuksxonaRus' => $lyuksxonaRu
            ]
        );
    }

    public function actionBoook()
    {
        $model = new Boook();
        $username = $_POST["Boook"]["name"];
        $telephone = $_POST["Boook"]["telephone"];

        Yii::$app->telegram->sendMessage(['chat_id' => 534980735, 'text' => $username]);
        Yii::$app->telegram->sendMessage(['chat_id' => 534980735, 'text' => $telephone]);

        return $this->render('boook', [
            'model' => $model
        ]);
    }

    public function actionUz()
    {
        $videos = Video::find()->one();
        $gallery = Gallery::find()->all();
        $roomRu = Room::find()->where(['lang' => 'ru'])->all();

        Yii::$app->language = 'uz';
        return $this->render("index", [
            'language' => Yii::$app->language,
            'videos' => $videos,
            'roomRus' => $roomRu,
            'gallerys' => $gallery
        ]);
    }

    public function actionUzTibbiyot()
    {
        Yii::$app->language = "uz";
        $blogUz = Blog::find()->where(['lang' => 'uz'])->all();
        // $blogs = Blog::find()->all();

        return $this->render(
            'tibbiyot',
            ['blogUzs' => $blogUz]
        );

        // return $this->render('blog', ['blogUzs' => $blogUz]);
    }

    public function actionUzXizmat()
    {
        Yii::$app->language = "uz";
        $park = Parking::find()->limit(1)->one();
        $second = Parking::find()->orderBy(['id' => SORT_DESC])->one();
        // print_r($second);exit;
        return $this->render(
            'Xizmat',
            [
                'park' => $park,
                'second' => $second
            ]
        );
    }

    public function actionUzBesedka()
    {
        Yii::$app->language = "uz";
        $besedkaUz = Besedka::find()->where(['lang' => 'uz'])->all();
        // $besedka = Besedka::find()->all();
        return $this->render(
            'besedka',
            [
                'besedkaUzs' => $besedkaUz
            ]
        );
    }

    public function actionUzParkovka()
    {
        Yii::$app->language = "uz";
        $parkovkaUz = Parking::find()->where(['lang' => 'uz'])->all();
        // $parkovka = Parking::find()->all();
        return $this->render(
            'parkovka',
            [
                'parkovkaUzs' => $parkovkaUz
            ]
        );
    }

    public function actionUzRestaurant()
    {
        Yii::$app->language = "uz";
        $gallery = Gallery::find()->all();

        $image = Restaurant::find()->limit(1)->one();
        $meals = Meal::find()->all();
        $mms = Meal::find()->orderBy(['id' => SORT_DESC])->all();
        return $this->render('restaurant', ['meals' => $meals, 'mms' => $mms, 'image' => $image, 'gallerys' => $gallery]);
    }

    public function actionRestaurant()
    {
        $image = Restaurant::find()->limit(1)->one();
        $gallery = Gallery::find()->all();

        $meals = Meal::find()->all();
        $mms = Meal::find()->orderBy(['id' => SORT_DESC])->all();
        return $this->render('restaurant', ['meals' => $meals, 'mms' => $mms, 'image' => $image, 'gallerys' => $gallery]);
    }

    public function actionUzBasseyn()
    {
        Yii::$app->language = "uz";
        // $basseyn = Basseyn::find();
        $basseynUz = Basseyn::find()->where(['lang' => 'uz'])->all();

        return $this->render(
            'basseyn',
            [
                'basseynUzs' => $basseynUz
            ]
        );
    }

    public function actionUzRaqsmaydoni()
    {
        Yii::$app->language = "uz";
        $raqsmaydoniUz = Raqsmaydoni::find()->where(['lang' => 'uz'])->all();
        // $raqsmaydoni = Raqsmaydoni::find()->all();
        return $this->render(
            'raqsmaydoni',
            [
                'raqsmaydoniUzs' => $raqsmaydoniUz
            ]
        );
    }

    public function actionUzBolalarmaydoni()
    {
        Yii::$app->language = "uz";
        $bolalarmaydoniUz = Bolalarmaydoni::find()->where(['lang' => 'uz'])->all();
        // $bolalarmaydoni = Bolalarmaydoni::find()->all();
        return $this->render(
            'bolalarmaydoni',
            [
                'bolalarmaydoniUzs' => $bolalarmaydoniUz
            ]
        );
    }

    public function actionUzJonliburchak()
    {
        Yii::$app->language = "uz";
        $jonliburchakUz = Jonliburchak::find()->where(['lang' => 'uz'])->all();

        // $jonliburchak = Jonliburchak::find()->all();
        return $this->render(
            'jonliburchak',
            [
                'jonliburchakUzs' => $jonliburchakUz
            ]
        );
    }


    public function actionUzSauna()
    {
        Yii::$app->language = "uz";
        $saunaUz = Sauna::find()->where(['lang' => 'uz'])->all();

        // $sauna = Sauna::find()->all();
        return $this->render(
            'sauna',
            [
                'saunaUzs' => $saunaUz
            ]
        );
    }


    public function actionUzContact()
    {
        Yii::$app->language = "uz";
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionUzBoook()
    {
        Yii::$app->language = "uz";
        $model = new Boook();
        return $this->render('boook', [
            'model' => $model,
        ]);
    }

    public function actionUzVipxona()
    {
        Yii::$app->language = "uz";
        $vipxonaUz = Vip::find()->where(['lang' => 'uz'])->all();
        // $vipxona = Vip::find()->all();
        return $this->render(
            'vipxona',
            [
                'vipxonaUzs' => $vipxonaUz
            ]
        );
    }

    public function actionUzLyuksxona()
    {
        Yii::$app->language = "uz";
        $lyuksxonaUz = Lyuks::find()->where(['lang' => 'uz'])->all();
        // $lyuksxona = Lyuks::find()->all();
        return $this->render(
            'lyuksxona',
            [
                'lyuksxonaUzs' => $lyuksxonaUz
            ]
        );
    }



    public function actionVideo()
    {
        $videoUz = Video::find()->where(['lang' => 'uz'])->all();
        $videoRu = Video::find()->where(['lang' => 'ru'])->all();
        // $videos = Video::find()->all();
        $model = new Video();
        return $this->render(
            'video',
            [
                'videoUzs' => $videoUz,
                'videoRus' => $videoRu
            ]
        );
    }

    public function actionUzVideo()
    {
        Yii::$app->language = "uz";
        $videoUz = Video::find()->where(['lang' => 'uz'])->all();
        // $videoUz = Video::find()->all();

        $model = new Video();
        return $this->render('video', [
            'videoUzs' => $videoUz
        ]);
    }
    public function actionVipkottej()
    {
        $vipkottejUz = Vipkottej::find()->where(['lang' => 'uz'])->all();
        $vipkottejRu = Vipkottej::find()->where(['lang' => 'ru'])->all();
        // $vipkottej = Vipkottej::find()->all();
        return $this->render(
            'vipkottej',
            [
                'vipkottejUzs' => $vipkottejUz,
                'vipkottejRus' => $vipkottejRu
            ]
        );
    }

    public function actionUzVipkottej()
    {
        Yii::$app->language = "uz";
        $vipkottejUz = Vipkottej::find()->where(['lang' => 'uz'])->all();

        // $vipkottej = Vipkottej::find()->all();
        return $this->render(
            'vipkottej',
            [
                'vipkottejUzs' => $vipkottejUz
            ]
        );
    }

    public function actionLyukskottej()
    {
        $lyukskottejUz = Lyukskottej::find()->where(['lang' => 'uz'])->all();
        $lyukskottejRu = Lyukskottej::find()->where(['lang' => 'ru'])->all();
        // $lyukskottej = Lyukskottej::find()->all();
        return $this->render(
            'lyukskottej',
            [
                'lyukskottejUzs' => $lyukskottejUz,
                'lyukskottejRus' => $lyukskottejRu,
            ]
        );
    }

    public function actionUzLyukskottej()
    {
        Yii::$app->language = "uz";
        $lyukskottejUz = Lyukskottej::find()->where(['lang' => 'uz'])->all();

        // $lyukskottej = Lyukskottej::find()->all();
        return $this->render(
            'lyukskottej',
            [
                'lyukskottejUzs' => $lyukskottejUz
            ]
        );
    }

    public function actionKottej4x()
    {

        $kottej4xRu = Kottej4x::find()->where(['lang' => 'ru'])->all();
        // $kottej4x = Kottej4x::find()->all();
        return $this->render(
            'kottej4x',
            [
                'kottej4xRus' => $kottej4xRu,
            ]
        );
    }

    public function actionUzKottej4x()
    {
        Yii::$app->language = "uz";
        $kottej4xUz = Kottej4x::find()->where(['lang' => 'uz'])->all();
        // $kottej4x = Kottej4x::find()->all();
        return $this->render(
            'kottej4x',
            [
                'kottej4xUzs' => $kottej4xUz
            ]
        );
    }

    public function actionKottej5x()
    {
        $kottej5xUz = Kottej5x::find()->where(['lang' => 'uz'])->all();
        $kottej5xRu = Kottej5x::find()->where(['lang' => 'ru'])->all();
        // $kottej5x = Kottej5x::find()->all();
        return $this->render(
            'kottej5x',
            [
                'kottej5xUzs' => $kottej5xUz,
                'kottej5xRus' => $kottej5xRu
            ]
        );
    }

    public function actionUzKottej5x()
    {
        Yii::$app->language = "uz";
        $kottej5xUz = Kottej5x::find()->where(['lang' => 'uz'])->all();
        // $kottej5x = Kottej5x::find()->all();
        return $this->render(
            'kottej5x',
            [
                'kottej5xUzs' => $kottej5xUz
            ]
        );
    }

    public function actionKottej6x()
    {
        $kottej6xUz = Kottej6x::find()->where(['lang' => 'uz'])->all();
        $kottej6xRu = Kottej6x::find()->where(['lang' => 'ru'])->all();
        // $kottej6x = Kottej6x::find()->all();
        return $this->render(
            'kottej6x',
            [
                'kottej6xUzs' => $kottej6xUz,
                'kottej6xRus' => $kottej6xRu
            ]
        );
    }

    public function actionUzKottej6x()
    {
        Yii::$app->language = "uz";
        $kottej6xUz = Kottej6x::find()->where(['lang' => 'uz'])->all();
        // $kottej6x = Kottej6x::find()->all();
        return $this->render(
            'kottej6x',
            [
                'kottej6xUzs' => $kottej6xUz
            ]
        );
    }

    public function actionKottej7x()
    {
        $kottej7xUz = Kottej7x::find()->where(['lang' => 'uz'])->all();
        $kottej7xRu = Kottej7x::find()->where(['lang' => 'ru'])->all();
        // $kottej7x = Kottej7x::find()->all();
        return $this->render(
            'kottej7x',
            [
                'kottej7xUzs' => $kottej7xUz,
                'kottej7xRus' => $kottej7xRu
            ]
        );
    }

    public function actionUzKottej7x()
    {
        Yii::$app->language = "uz";
        $kottej7xUz = Kottej7x::find()->where(['lang' => 'uz'])->all();
        // $kottej7x = Kottej7x::find()->all();
        return $this->render(
            'kottej7x',
            [
                'kottej7xUzs' => $kottej7xUz
            ]
        );
    }

    public function actionKottej8x()
    {
        $kottej8xUz = Kottej8x::find()->where(['lang' => 'uz'])->all();
        $kottej8xRu = Kottej8x::find()->where(['lang' => 'ru'])->all();
        // $kottej8x = Kottej8x::find()->all();
        return $this->render(
            'kottej8x',
            [
                'kottej8xUzs' => $kottej8xUz,
                'kottej8xRus' => $kottej8xRu
            ]
        );
    }

    public function actionUzKottej8x()
    {
        Yii::$app->language = "uz";
        $kottej8xUz = Kottej8x::find()->where(['lang' => 'uz'])->all();
        // $kottej8x = Kottej8x::find()->all();
        return $this->render(
            'kottej8x',
            [
                'kottej8xUzs' => $kottej8xUz
            ]
        );
    }
}
