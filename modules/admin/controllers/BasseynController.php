<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Basseyn;
use app\models\search\BasseynSearch;
use yii\web\UploadedFile;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BasseynController implements the CRUD actions for Basseyn model.
 */
class BasseynController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Basseyn models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BasseynSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Basseyn model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Basseyn model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Basseyn();

        if ($model->load(Yii::$app->request->post())) {
            // var_dump($_POST);
            $img = UploadedFile::getInstance($model, 'image');
            if (!empty($img)) {
                $model->image = random_int(0, 9999) . '.' . $img->extension;
            }


            if ($model->save()) {
                if (!empty($img)) {
                    $img->saveAs('uploads/basseyn/' . $model->image);
                    return $this->redirect(['index']);
                }
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Basseyn model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $img = UploadedFile::getInstance($model, 'image');

            if (!empty($img)) {
                $model->image = random_int(0, 9999) . '.' . $img->extension;
            }

            if ($model->save()) {
                if (!empty($img)) {
                    $img->saveAs('uploads/basseyn/' . $model->image);
                    return $this->redirect(['index']);
                }
                return $this->redirect(['index']);
            }

           
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Basseyn model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $data = Basseyn::findOne($id);
        unlink(Yii::$app->basePath . '/public_html/uploads/basseyn/' . $data->image);

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Basseyn model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Basseyn the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Basseyn::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
