<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Bolalarmaydoni;
use app\models\search\BolalarmaydoniSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * BolalarmaydoniController implements the CRUD actions for Bolalarmaydoni model.
 */
class BolalarmaydoniController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Bolalarmaydoni models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BolalarmaydoniSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Bolalarmaydoni model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Bolalarmaydoni model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Bolalarmaydoni();

        if ($model->load(Yii::$app->request->post())) {
            $img = UploadedFile::getInstance($model, 'image');
            if (!empty($img)) {
                $model->image = random_int(0, 9999) . '.' . $img->extension;
            }


            if ($model->save()) {
                if (!empty($img)) {
                    $img->saveAs('uploads/bolalarmaydoni/' . $model->image);
                    return $this->redirect(['index']);
                }
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Bolalarmaydoni model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $img = UploadedFile::getInstance($model, 'image');

            if (!empty($img)) {
                $model->image = random_int(0, 9999) . '.' . $img->extension;
            }

            if ($model->save()) {
                if (!empty($img)) {
                    $img->saveAs('uploads/bolalarmaydoni/' . $model->image);
                    return $this->redirect(['index']);
                }
                return $this->redirect(['index']);
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Bolalarmaydoni model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $data = Bolalarmaydoni::findOne($id);
        unlink(Yii::$app->basePath . '/public_html/uploads/bolalarmaydoni/' . $data->image);
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Bolalarmaydoni model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Bolalarmaydoni the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Bolalarmaydoni::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
