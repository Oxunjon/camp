<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Bolalarmaydoni */

$this->title = 'Kiritish';
$this->params['breadcrumbs'][] = ['label' => 'Bolalarmaydoni', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="Bolalarmaydoni-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
