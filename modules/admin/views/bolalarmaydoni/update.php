<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Bolalarmaydoni */

$this->title = 'Bolalarmaydonini Tahrirlash: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Bolalarmaydoni', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Tahrirlash';
?>
<div class="Bolalarmaydoni-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
