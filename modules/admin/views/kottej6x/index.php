<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\Kottej6xSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kottej6x';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kottej6x-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Kottej6x', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            'text:ntext',
            'lang',
            [
                'attribute' => 'image',
                'format' => 'html',
                'value' => function ($model) {

                    return \yii\helpers\Html::img($model->getImage(), ['height' => 70, 'width' => 150]);
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
