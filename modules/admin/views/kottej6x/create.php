<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Kottej6x */

$this->title = 'Kiritish';
$this->params['breadcrumbs'][] = ['label' => 'Kottej6x', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kottej6x-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
