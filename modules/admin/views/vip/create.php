<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Vip */

$this->title = 'Kiritish';
$this->params['breadcrumbs'][] = ['label' => 'Vip', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vip-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
