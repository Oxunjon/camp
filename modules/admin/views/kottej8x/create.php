<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Kottej8x */

$this->title = 'Kiritish';
$this->params['breadcrumbs'][] = ['label' => 'Kottej8x', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kottej8x-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
