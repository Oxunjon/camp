<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Besedka */

$this->title = 'Create Besedka';
$this->params['breadcrumbs'][] = ['label' => 'Besedkas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="besedka-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
