<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Kottej */

$this->title = 'Tahrirlash: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Kottejs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kottej-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
