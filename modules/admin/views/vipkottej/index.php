<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\db\ActiveRecord;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\KottejSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Vip kottej';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kottej-index">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Kiritish', ['create'], ['class' => 'btn btn-success']) ?>
    </p>



    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            'lang',
            [
                'attribute' => 'image',
                'format' => 'html',
                'value' => function ($model) {

                    return \yii\helpers\Html::img($model->getImage(), ['height' => 70, 'width' => 150]);
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
