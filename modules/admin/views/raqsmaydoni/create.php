<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Raqsmaydoni */

$this->title = 'Raqsmaydonini kiritish';
$this->params['breadcrumbs'][] = ['label' => 'Raqsmaydoni', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="Raqsmaydoni-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
