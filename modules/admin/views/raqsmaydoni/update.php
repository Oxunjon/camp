<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Raqsmaydoni */

$this->title = 'Tahrirlash: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Raqsmaydoni', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Tahrirlash';
?>
<div class="Raqsmaydoni-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
