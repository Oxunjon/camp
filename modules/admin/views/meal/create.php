<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Meal */

$this->title = 'Oshxonani kiritish';
$this->params['breadcrumbs'][] = ['label' => 'Oshxona', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meal-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
