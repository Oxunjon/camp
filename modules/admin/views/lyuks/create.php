<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Lyuks */

$this->title = 'Kiritish';
$this->params['breadcrumbs'][] = ['label' => 'Lyuks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lyuks-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
