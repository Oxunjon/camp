<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Lyuks */

$this->title = 'Tahrirlash: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Lyuks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lyuks-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
