<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Kottej */

$this->title = 'Kottejni kiritish';
$this->params['breadcrumbs'][] = ['label' => 'Kottejlar', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kottej-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
