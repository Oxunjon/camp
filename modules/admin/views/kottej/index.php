<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\db\ActiveRecord;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\KottejSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Kottejlar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kottej-index">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Kottejni kiritish', ['create'], ['class' => 'btn btn-success']) ?>
    </p>



    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'image',
            [   
                'attribute'=>'text',
                'format'=>'raw'
            ],
            // 'text:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
