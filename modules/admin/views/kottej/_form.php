<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;
use \yii\helpers\ArrayHelper;
use \app\models\Kottej;

/* @var $this yii\web\View */
/* @var $model app\models\Kottej */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="kottej-form">

    <?php $form = ActiveForm::begin(); ?>

<!--    --><?php
//    Html::dropDownList($model,'IsActive',array(""=>"Select","1"=>"Active", "2"=>"Non Active"));
//    ?>
    <?= $form->field($model,'name')->dropDownList(
   ArrayHelper::map(Kottej::find()->all(),'id','title'),['prompt'=>"Select Kottej"]
     );?>


    <?= $form->field($model, 'img')->fileInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'text')->widget(TinyMce::className(), [
    'options' => ['rows' => 6],
    'language' => 'en',
    'clientOptions' => [
        'plugins' => [
            "advlist autolink lists link charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
        'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
    ]
]);?>

    <div class="form-group">
        <?= Html::submitButton('Saqlash', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
