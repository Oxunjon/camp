<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LyuksKottej */

$this->title = 'Kiritish';
$this->params['breadcrumbs'][] = ['label' => 'Lyuks Kottejs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lyuks-kottej-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
