<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Jonliburchak */

$this->title = 'Jonliburchak: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Jonliburchaks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Tahrirlash';
?>
<div class="jonliburchak-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
