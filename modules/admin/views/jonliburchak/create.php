<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Jonliburchak */

$this->title = 'Jonliburchak';
$this->params['breadcrumbs'][] = ['label' => 'Jonliburchak', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jonliburchak-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
