<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Kottej5x */

$this->title = 'Create Kottej5x';
$this->params['breadcrumbs'][] = ['label' => 'Kottej5xes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kottej5x-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
