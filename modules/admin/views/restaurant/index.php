<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\RestaurantSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Oshxona';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="restaurant-index">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Kiritish', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'name',
            'text:ntext',
            'lang',
            [
                'attribute' => 'image',
                'format' => 'html',
                'value' => function ($model) {

                    return \yii\helpers\Html::img($model->getImage(), ['height' => 70, 'width' => 150]);
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
