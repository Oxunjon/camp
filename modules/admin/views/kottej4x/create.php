<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Kottej4x */

$this->title = 'Kiritish';
$this->params['breadcrumbs'][] = ['label' => 'Kottej 4x', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kottej4x-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
