<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Kottej7x */

$this->title = 'Kiritish';
$this->params['breadcrumbs'][] = ['label' => 'Kottej7x', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kottej7x-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
