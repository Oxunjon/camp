<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\Sauna */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sauna';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sauna-index">

    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Saunani kiritish', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            'text:ntext',
            'lang',
            [
                'attribute'=>'text',
                'format'=>'raw'
            ],
            [
                'attribute' => 'image',
                'format' => 'html',
                'value' => function ($model) {

                    return \yii\helpers\Html::img($model->getImage(), ['height' => 70, 'width' => 150]);
                }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
