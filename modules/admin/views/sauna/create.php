<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Sauna */

$this->title = 'Kiritish';
$this->params['breadcrumbs'][] = ['label' => 'Sauna', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sauna-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
