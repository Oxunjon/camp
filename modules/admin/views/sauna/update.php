<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Sauna */

$this->title = 'tahrirlash: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Sauna', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sauna-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
