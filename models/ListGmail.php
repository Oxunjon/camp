<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "list_gmail".
 *
 * @property int $id
 * @property string $gmail
 * @property string $telegram_chat_id
 * @property int $status
 */
class ListGmail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'list_gmail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['gmail','telegram_chat_id'], 'required'],
            [['status'], 'integer'],
            [['gmail'], 'string', 'max' => 255],
            [['telegram_chat_id'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'gmail' => 'Gmail',
            'telegram_chat_id' => 'Telegram Id',
            'status' => 'Status',
        ];
    }
}
