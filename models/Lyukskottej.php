<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lyukskottej".
 *
 * @property int $id
 * @property string $title
 * @property string $lang
 * @property string $text
 * @property string $image
 */
class Lyukskottej extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $img;
    public static function tableName()
    {
        return 'lyukskottej';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'text','lang', 'image'], 'required'],
            [['text'], 'string'],
            [['title','lang'], 'string', 'max' => 300],
            [['image'],  'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg,gif,jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lang'=>'Tilni tanlang',
            'title' => 'Sarlavhani yozing',
            'text' => 'Matnni kiriting',
            'image' => 'Rasmni tanlang',
        ];
    }
    public function getImage()
    {
        return ($this->image) ? '@web/uploads/lyukskottej/' . $this->image : '@web/uploads/lyukskottej/';
    }
}
