<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vip".
 *
 * @property int $id
 * @property string $title
 * @property string $lang
 * 
 * @property string $text
 * @property string $image
 */
class Vip extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $img;
    public static function tableName()
    {
        return 'vip';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'text','lang', 'image'], 'required'],
            [['text','lang'], 'string'],
            [['title'], 'string', 'max' => 300],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png,jpg,gif,jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lang'=>'Tilni tanlang',
            'title' => 'Sarlavha',
            'text' => 'Matnni kiriting',
            'image' => 'Rasm',
        ];
    }
    public function getImage()
    {
        return ($this->image) ? '@web/uploads/vip/' . $this->image : '@web/uploads/vip/';
    }
}
