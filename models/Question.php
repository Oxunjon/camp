<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "question".
 *
 * @property int $id
 * @property string $name
 * @property string $phone_or_telegram
 * @property int $view
 * @property int $send
 */
class Question extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'question';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'phone_or_telegram'], 'required'],
            [['view', 'send'], 'integer'],
            [['name', 'phone_or_telegram'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => Yii::t('template','Ism'),
            'phone_or_telegram' => 'Телефон или телеграм',
            'view' => 'View',
            'send' => 'Send',
        ];
    }
}
