<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "restaurant".
 *
 * @property int $id
 * @property string $name
 * @property string $lang
 * @property string $text
 * @property string $image
 */
class Restaurant extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $img;
    public static function tableName()
    {
        return 'restaurant';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'text', 'lang', 'image'], 'required'],
            [['text','lang'], 'string'],
            [['name'], 'string', 'max' => 300],
            [['image'], 'string', 'max' => 255],
            [['img'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg,gif,jpeg']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lang' => 'Tilni tanlang',

            'name' => 'Nomi',
            'text' => 'Matnni kiriting',
            'image' => 'Rasm',
        ];
    }
    public function getImage()
    {
        return ($this->image) ? '@web/uploads/restaurant/' . $this->image : '@web/uploads/restaurant/';
    }
}
