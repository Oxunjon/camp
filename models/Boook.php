<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "boook".
 *
 * @property string $name
 * @property string $telephone
 */
class Boook extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'boook';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name',  'telephone'], 'required'],
            [['name'], 'string'],
            [['telephone'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'telephone' => 'Telephone',
        ];
    }
}
