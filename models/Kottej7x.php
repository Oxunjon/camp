<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kottej7x".
 *
 * @property int $id
 * @property string $title
 * @property string $lang
 * 
 * @property string $text
 * @property string $image
 */
class Kottej7x extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $img;
    public static function tableName()
    {
        return 'kottej7x';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'text', 'lang','image'], 'required'],
            [['text'], 'string'],
            [['title','lang'], 'string', 'max' => 300],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg,gif,jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lang'=>'Tilni tanlang',
            'title' => 'Sarlavhani yozing',
            'text' => 'Matinni kiriting',
            'image' => 'Rasmni tanlang',
        ];
    }
    public function getImage()
    {
        return ($this->image) ? '@web/uploads/kottej7x/' . $this->image : '@web/uploads/kottej7x/';
    }
}
