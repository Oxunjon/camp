<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "room".
 *
 * @property int $id
 * @property string $name
 * @property string $lang
 * @property string $price
 * @property string $small_text
 * @property string $image
 * @property string $text
 */
class Room extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $img;
    public static function tableName()
    {
        return 'room';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'price', 'small_text', 'lang','image', 'text'], 'required'],
            [['small_text', 'text','lang'], 'string'],
            [['name', 'price', 'image'], 'string', 'max' => 255],
            [['img'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg,gif,jpeg']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lang' => 'Tilni tanlang',

            'name' => 'Nomi',
            'price' => 'Summasi',
            'small_text' => 'Sarlavha',
            'image' => 'Rasm',
            'text' => 'Matnni kiriting',
        ];
    }
    public function getImage()
    {
        return ($this->image) ? '@web/uploads/room/' . $this->image : '@web/uploads/room/';
    }
}
