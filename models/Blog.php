<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "blog".
 *
 * @property int $id
 * @property string $title
 * @property string $lang
 * @property string $text
 * @property string $image
 */
class Blog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $img;
    public static function tableName()
    {
        return 'blog';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'text', 'lang', 'image'], 'required'],
            [['text'], 'string'],
            [['title','lang'], 'string', 'max' => 300],
            [['image'], 'string', 'max' => 255],
            [['img'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg,gif,jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lang' => 'Tilni tanlang',
            'title' => 'Sarlavha yozing',
            'text' => 'Matnni kiriting',
            'image' => 'Rasmni kiriting',

        ];
    }
    public function getImage()
    {
        return ($this->image) ? '@web/uploads/blog/' . $this->image : '@web/uploads/blog/';
    }
}
