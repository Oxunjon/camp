<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sauna".
 *
 * @property int $id
 * @property string $title
 * @property string $lang
 * @property string $text
 * @property string $image
 */
class Sauna extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $img;
    public static function tableName()
    {
        return 'sauna';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['image', 'title','lang', 'text'], 'required'],
            [['text','lang'], 'string'],
            [['title'], 'string', 'max' => 300],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png,jpg,gif,jpeg'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lang' => 'Tilni tanlang',

            'title' => 'Sarlavha',
            'text' => 'Matn',
            'image' => 'Rasm',
        ];
    }
    public function getImage()
    {
        return ($this->image) ? '@web/uploads/sauna/' . $this->image : '@web/uploads/sauna/';
    }
}
