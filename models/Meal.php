<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "meal".
 *
 * @property int $id
 * @property string $name
 * @property string $lang
 * @property string $image
 * @property string $text
 */
class Meal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $img;
    public static function tableName()
    {
        return 'meal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'image','lang', 'text'], 'required'],
            [['text','lang'], 'string'],
            [['name', 'image'], 'string', 'max' => 255],
            [['img'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg,gif,jpeg']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lang' => 'Tilni tanlang',

            'name' => 'Nomini kiriting',
            'image' => 'Rasmni kiriting',
            'text' => 'Matnni kiriting',
        ];
    }
    public function getImage()
    {
        return ($this->image) ? '@web/uploads/meal/' . $this->image : '@web/uploads/meal/';
    }
}
