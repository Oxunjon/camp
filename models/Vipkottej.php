<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vipkottej".
 *
 * @property int $id
 * @property string $title
 * @property string $image
 * @property string $text
 */
class Vipkottej extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $img;
    public static function tableName()
    {
        return 'vipkottej';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'image', 'lang','text'], 'required'],
            [['text','lang'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['img'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg,gif,jpeg', 'maxSize' => 1024 * 1024 * 10]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lang'=>'Tilni tanlang',
            'title' => 'Sarlavha',
            'image' => 'Rasm',
            'text' => 'Matn',
        ];
    }
    public function getImage()
    {
        return ($this->image) ? '@web/uploads/vipkottej/' . $this->image : '@web/uploads/vipkottej/';
    }
}
