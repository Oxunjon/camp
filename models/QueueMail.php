<?php


namespace common\models;


use yii\base\BaseObject;
use yii\helpers\Html;
use yii\queue\JobInterface;

class QueueMail extends BaseObject implements JobInterface
{
    public $name;
    public $phone_or_telegram;

    public function execute($queue)
    {
        $this->email($this->name,$this->phone_or_telegram);
    }

    private function email($name,$phone_or_telegram){

        $message = $name . '<br>' . $phone_or_telegram;

        $listMails = \common\models\ListGmail::find()->all();
        foreach ($listMails as $item){
            \Yii::$app->mail->compose()
                ->setFrom([\Yii::$app->params['senderEmail'] => \Yii::$app->params['senderName'] ])
                ->setTo($item->gmail)
                ->setSubject('ugamoromgohi.uz')
                ->setHtmlBody($message)
                ->send();

            sleep(10);
        }


    }

}