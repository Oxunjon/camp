<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gallery".
 *
 * @property int $id
 * @property string $parking_image
 * @property string $kottej_image
 * @property string $blog_image
 */
class Gallery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $parking;
    public $kottej;
    public $blog;
    public static function tableName()
    {
        return 'gallery';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parking_image', 'kottej_image', 'blog_image'], 'required'],
            [['parking_image', 'kottej_image', 'blog_image'], 'string', 'max' => 255],
            [['parking'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg,gif,jpeg'],
            [['kottej'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg,gif,jpeg'],
            [['blog'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg,gif,jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parking_image' => 'Rasmni kiriting',
            'kottej_image' => 'Rasmni kiriting',
            'blog_image' => 'Rasmni kiriting',
        ];
    }
    public function getImage()
    {
        return ($this->image) ? '@web/uploads/gallery/' . $this->image : '@web/uploads/gallery/';
    }
}
