<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "video".
 *
 * @property int $id
 * @property string $name
 * @property string $lang
 * @property string $title
 * @property string $image
 * @property string $link
 * @property string $text
 */
class Video extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $img;
    public static function tableName()
    {
        return 'video';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'title','lang', 'text', 'link'], 'required'],
            [['text','lang'], 'string'],
            [['name', 'title', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lang'=>'Tilni tanlang',
            'name' => 'Nomi',
            'title' => 'Sarlavha',
            'text' => 'Matn',
            'link' => 'Linkni kiriting',
        ];
    }
    public function getImage()
    {
        return ($this->image) ? '@web/uploads/video/' . $this->image : '@web/uploads/video/';
    }
}
